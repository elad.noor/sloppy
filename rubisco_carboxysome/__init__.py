CARBON_SOURCES_LIST = [
    ('ac', ),
    ('succ', ),
    ('glc__D', ),
    ('f6p', ),
    ('6pgc', ),
    ('r5p', ),
    ('xu5p__D', ),
    ('2pg', ),
    ('dhap', ),
]

SINGLE_KOS = [
    'GLCptspp',
    'PGI',
    'PFK',
    'FBP',
    'FBA',
    'TPI',
    'GAPD|PGK',
    'PGM',
    'ENO',
    'PYK',
    'PPS',
    'PDH',
    'PFL',
    'G6PDH2r|PGL',
    'GND',
    'EDD|EDA',
    'RPE',
    'RPI',
    'TKT1|TKT2',
    'TALA',
    'PPC',
    'PPCK',
    'ICL',
    'MALS',
    'CS',
    'ACONTa|ACONTb',
    'ALCD2x',
    'ACALD',
]

TARGET_REACTION = 'RBC'

WILDTYPE_MODEL = "core_model_with_rpp_and_fdh.xml"

DEFAULT_MAX_N_KO = 2
