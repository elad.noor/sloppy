import ged
from cobra.io import read_sbml_model
from ged import WILDTYPE_MODEL
from importlib.resources import files


wt_model = read_sbml_model(files("ged").joinpath(WILDTYPE_MODEL).read_text())

wt_model.reactions.EX_co2_e.bounds = (-1000, 1000)
wt_model.reactions.EX_for_e.bounds = (-1000, 1000)
wt_model.reactions.EX_glc_e.bounds = (0, 0)

res = wt_model.optimize()
print(res)
