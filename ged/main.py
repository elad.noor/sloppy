from itertools import combinations, compress, product
from typing import List, Tuple

import pandas as pd
from cobra import Model
from cobra.io import read_sbml_model
from importlib.resources import files
from optslope import calculate_slope, filter_redundant_knockouts
from tqdm.contrib.concurrent import process_map
from path import Path
from functools import partial

from . import (
    CARBON_SOURCES_LIST,
    DEFAULT_MAX_N_KO,
    SINGLE_KOS,
    TARGET_REACTION,
    WILDTYPE_MODEL,
)

def check_autotrophic_growth(model: Model, knockouts: List[str]) -> bool:
    ko_model = model.copy()
    for ko in knockouts:
        for k in ko.split("|"):
            ko_model.reactions.get_by_id(k).knock_out()
    ko_model.reactions.EX_for_e.bounds = (-1000, 1000)

    res = ko_model.optimize()
    return res.status == "optimal" and res.objective_value > 1e-2

def calculate_slope_for_ko(model: Model, args: Tuple[List[str], List[str]]):
    knockouts, carbon_sources = args
    return calculate_slope(
        wt_model=model,
        knockouts=knockouts,
        carbon_sources=carbon_sources,
        target_reaction=TARGET_REACTION,
    )

def main(max_knockouts: int = DEFAULT_MAX_N_KO, num_processes: int = 1) -> None:
    FULL_RESULT_PATH = str(
        Path(__file__).parent / f"results_full_{max_knockouts}KOs.csv"
    )
    SUMMARY_PATH = str(
        Path(__file__).parent / f"results_summary_{max_knockouts}KOs.csv"
    )

    wt_model = read_sbml_model(files("ged").joinpath(WILDTYPE_MODEL).read_text())

    # make a list of all KOs where autotrophic growth is still possible
    all_knockouts = [
        knockouts
        for n_kos in range(max_knockouts + 1)
        for knockouts in combinations(SINGLE_KOS, n_kos)
    ]


    # use the `check_autotrophic_growth` function to filter out all KOs that are not
    # compatible with autotrophic growth (and therefore we don't need to explore them
    # further)
    print(
        f"Checking for autotrophic growth for all combinations of "
        f"up to {max_knockouts} knockouts and keeping only those that can grow."
    )

    autotrophic_knockouts = compress(
        all_knockouts,
        process_map(
            partial(check_autotrophic_growth, wt_model),
            all_knockouts,
            chunksize=100,
            max_workers=num_processes,
        ),
    )

    # generate a list of tuples with all combinations of KOs and carbon sources using
    # a cartesian product of the two lists
    knockouts_list, carbon_sources_list = zip(
        *product(autotrophic_knockouts, CARBON_SOURCES_LIST)
    )
    knockouts_list = list(knockouts_list)
    carbon_sources_list = list(carbon_sources_list)


    print(
        f"Calculating slopes for all combinations of autotrophic knockouts, and "
        f"any of the {len(CARBON_SOURCES_LIST)} carbon sources"
    )
    slope_list = process_map(
        partial(calculate_slope_for_ko, wt_model),
        zip(knockouts_list, carbon_sources_list),
        chunksize=100,
        max_workers=num_processes,
    )
    result_df = pd.DataFrame(
        data=zip(knockouts_list, carbon_sources_list, slope_list),
        columns=["knockouts", "carbon_sources", "slope"],
    )
    result_df.carbon_sources = result_df.carbon_sources.str.join(" + ")
    result_df = result_df.round(3).fillna(-1.0)

    # write all the slopes to a CSV file
    with open(FULL_RESULT_PATH, "w") as fp:
        result_df.to_csv(fp)

    pivot_df = filter_redundant_knockouts(result_df)

    # reorder the columns to match the original list order
    col_order = list(map(" + ".join, CARBON_SOURCES_LIST)) + [
        "smallest_slope",
        "highest_slope",
        "slope_ratio",
        "no_knockouts",
    ]
    pivot_df = pivot_df[col_order]

    # convert the list of knockouts into a simple string
    pivot_df.index = pivot_df.index.str.join("|")

    # write summary of results to CSV file
    with open(SUMMARY_PATH, "w") as fp:
        pivot_df.round(3).to_csv(fp)
