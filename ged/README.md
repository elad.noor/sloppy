## Changes to the E. coli core wt_mode:
We made the following changes to the E. coli core mode <REFERENCE>:
- Adding the Entner-Doudoroff pathway (EDA and EDD)
- Adding import reactions for: formate, glycerol, fructose, lactate, succinate, glycerate
- Making the trans-hydrogenase reaction (NADTRHD) reversible
- Adding the reverse reaction for GND (denoted GND_r)
- Adding the NAD-dependent formate dehydrogenase reaction (FDH)
