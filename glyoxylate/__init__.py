# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

ICH360 = True

if ICH360:

    CARBON_SOURCES_LIST = [
        ('glx', 'succ', ),
        ('glx', 'glyc', ),
    ]

    SINGLE_KOS = [
        'EDD',
        'EDA',
        'PGI',
        'PFK',
        'FBP',
        'FBA',
        'TPI',
        'PGK',
        'GAPD',
        'PGM',
        'ENO',
        'PYK',
        'PPS',
        'PDH',
        'PFL',
        'G6PDH2r',
        'PGL',
        'GND',
        'RPE',
        'RPI',
        'TKT1|TKT2',
        'TALA',
        'PPC',
        'PPCK',
        'ME1|ME2',
        'CS',
        'ACONTa|ACONTb',
        'ICDHyr',
        'ICL',
        'MALS',
        'AKGDH',
        'SUCOAS',
        'SUCDi',
        'FUM',
        'MDH',
        'ALCD2x',
        'ACALD',
        'GLXCL',
        'GHMT2r',
    ]
    TARGET_REACTION = "glx_t"

    WILDTYPE_MODEL = "Escherichia_coli_iCH360.xml"

    DEFAULT_MAX_N_KO = 2

else:  # use the core model

    CARBON_SOURCES_LIST = [
        # ('glx', 'fru', ),
        # ('glx', 'r5p', ),
        ('glx', 'succinate',),
        # ('glx', '2pg', ),
        # ('glx', 'ac', ),
        # ('glx', 'dhap', ),
        ('glx', 'glycerol',),
        # ('glx', 'glycerate', ),
    ]

    SINGLE_KOS = [
        'GLPX',
        'GLCpts',
        'EDD|EDA',
        'PGI',
        'PFK|PFK2',
        'FBP',
        'FBA|FBA2',
        'TPI',
        'PGK',  # coupled to ΔGAPD
        'PGM',
        'ENO',
        'PYK',
        'PPS',
        'PDH',
        'PFL',
        'G6PDH2r',
        'PGL',  # coupled to ΔGND
        'RPE',
        'RPI',
        'TKT1|TKT2',
        'TALA',
        'PPC',
        'PPCK',
        'ME1|ME2',
        'CS',  # coupled to ΔACONTa ΔACONTb
        'ICDHyr',
        'ICL',  # coupled to ΔMALS
        'AKGDH',  # coupled to ΔSUCOAS
        'SUCDi',
        'FRD7',
        'FUM',
        'MDH',
        'ALCD2x',
        'ACALD'
    ]

    TARGET_REACTION = "glx_t"

    WILDTYPE_MODEL = "core_model_extended.xml"

    DEFAULT_MAX_N_KO = 3
