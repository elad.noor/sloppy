# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import logging
import os

import biosensors.main as _biosensors
import click
import click_log
import ged.main as _ged
import methanol.main as _methanol
import rubisco.main as _rubisco
import rubisco_bsubtilis.main as _rubisco_bsubtilis
import rubisco_carboxysome.main as _rubisco_carboxysome
import glyoxylate.main as _glyoxylate

logger = logging.getLogger()
click_log.basic_config(logger)

try:
    DEFAULT_NUM_PROCESSES = max(1, len(os.sched_getaffinity(0)) - 1)
except OSError:
    logger.warning("Could not determine the number of cores available - assuming 1.")
    DEFAULT_NUM_PROCESSES = 1

try:
    # supress the many annoying debug messages that Gurobi generates
    import gurobipy
    gurobipy.setParam('OutputFlag', 0)
except ImportError:
    pass


@click.group()
@click.help_option("--help", "-h")
@click_log.simple_verbosity_option(
    logger,
    default="INFO",
    show_default=True,
    type=click.Choice(["CRITICAL", "ERROR", "WARN", "INFO", "DEBUG"]),
)
def cli():
    """
    Command line interface
    """
    pass


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "-k",
    metavar="MAX_KO",
    default=3,
    show_default=True,
    help="Maximum number of knockouts to consider for each mutant",
)
@click.option(
    "-j",
    metavar="NUM_PROCESSES",
    default=DEFAULT_NUM_PROCESSES,
    show_default=True,
    help="number of processes to use in parallel (should be roughly no. of "
    "CPU cores)",
)
@click.option(
    "--core",
    metavar="CORE",
    default=False,
    show_default=True,
    help="If True, use the E. coli core model (default: iML1515)",
)
def methanol(k: int, j: int, core: bool):
    """Calculate a DataFrame with slopes of all KOs on all CSs."""
    _methanol.main(max_ko=k, num_processes=j, core=core)


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "-j",
    metavar="NUM_PROCESSES",
    default=DEFAULT_NUM_PROCESSES,
    show_default=True,
    help="number of processes to use in parallel (should be roughly no. of "
    "CPU cores)",
)
@click.option(
    "-k",
    metavar="MAX_KO",
    default=_ged.DEFAULT_MAX_N_KO,
    show_default=True,
    help="Maximum number of knockouts to consider for each mutant",
)
def ged(j: int, k: int):
    """Calculate a DataFrame with slopes of all KOs on all CSs."""
    _ged.main(max_knockouts=k, num_processes=j)


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "-j",
    metavar="NUM_PROCESSES",
    default=DEFAULT_NUM_PROCESSES,
    show_default=True,
    help="number of processes to use in parallel (should be roughly no. of "
    "CPU cores)",
)
@click.option(
    "-k",
    metavar="MAX_KO",
    default=_biosensors.DEFAULT_MAX_N_KO,
    show_default=True,
    help="Maximum numbe r of knockouts to consider for each mutant",
)
def biosensors(j: int, k: int):
    """Calculate a DataFrame with slopes of all KOs on all CSs."""
    _biosensors.main(max_k=k, num_processes=j)


@cli.command(
    help="This script recreates a 2D heatmap of all double knockouts "
    "and their slopes, as shown in figure S3(a) in Antonovsky "
    "et al. (Cell 2016)"
)
@click.help_option("--help", "-h")
@click.option(
    "-k",
    metavar="MAX_KO",
    default=_rubisco.DEFAULT_MAX_N_KO,
    show_default=True,
    help="Maximum number of knockouts to consider for each mutant",
)
@click.option(
    "-j",
    metavar="NUM_PROCESSES",
    default=DEFAULT_NUM_PROCESSES,
    show_default=True,
    help="number of processes to use in parallel (should be roughly no. of "
    "CPU cores)",
)
def rubisco(k: int, j: int):
    """Calculate a DataFrame with slopes of all KOs on all CSs."""
    logging.info(
        f"Running 'rubisco' with {max_ko} KOs and " f"{num_processes} processes"
    )
    _rubisco.main(max_ko=k, num_processes=j)


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "-k",
    metavar="MAX_KO",
    default=_rubisco_bsubtilis.DEFAULT_MAX_N_KO,
    show_default=True,
    help="Maximum number of knockouts to consider for each mutant",
)
@click.option(
    "-j",
    metavar="NUM_PROCESSES",
    default=DEFAULT_NUM_PROCESSES,
    show_default=True,
    help="number of processes to use in parallel (should be roughly no. of "
    "CPU cores)",
)
def rubisco_bsubtilis(k: int, j: int):
    """Calculate a DataFrame with slopes of all KOs on all CSs."""
    _rubisco_bsubtilis.main(max_knockouts=k, num_processes=j)


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "-k",
    metavar="MAX_KO",
    default=_rubisco_carboxysome.DEFAULT_MAX_N_KO,
    show_default=True,
    help="Maximum number of knockouts to consider for each mutant",
)
@click.option(
    "-j",
    metavar="NUM_PROCESSES",
    default=DEFAULT_NUM_PROCESSES,
    show_default=True,
    help="number of processes to use in parallel (should be roughly no. of "
    "CPU cores)",
)
def rubisco_carboxysome(k: int, j: int):
    """Calculate a DataFrame with slopes of all KOs on all CSs."""
    _rubisco_carboxysome.main(max_ko=k, num_processes=j)


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "-k",
    metavar="MAX_KO",
    default=_glyoxylate.DEFAULT_MAX_N_KO,
    show_default=True,
    help="Maximum number of knockouts to consider for each mutant",
)
@click.option(
    "-j",
    metavar="NUM_PROCESSES",
    default=DEFAULT_NUM_PROCESSES,
    show_default=True,
    help="number of processes to use in parallel (should be roughly no. of "
    "CPU cores)",
)
def glyoxylate(k: int, j: int):
    """Calculate a DataFrame with slopes of all KOs on all CSs."""
    _glyoxylate.main(max_ko=k, num_processes=j)

if __name__ == "__main__":
    cli()
