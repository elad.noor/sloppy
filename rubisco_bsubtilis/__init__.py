from path import Path


WILDTYPE_MODEL_FNAME = "Bsub_core_rbc.xml"

DEFAULT_MAX_N_KO = 3

CARBON_SOURCES_LIST = [
    ("glc",),
    ("fru",),
    ("6pg",),
    ("r5p",),
    ("succ",),
    ("xyl",),
    ("2ga",),
    ("ac",),
    ("dhap",),
    ("for",),
]

SINGLE_KOS = """
TptsG
Epgi
Epfk
Efbp
Efba
Etpi
EgapA
EgapB
Epgk
Epgm
Eeno
Epyk
Epdh
Epta
EackA
EacsA
Ezwf
Egnd
EywlF
Erpe
Etkt1
Etkt2
EywjH
Epyc
EcitZ
EcitB
EcitC
Eodh
Esuc
Esdh
EcitG
Epck
EmalS
EytsJ
Endh
EyutJ
""".strip().split(
    "\n"
)

TARGET_REACTION = "Erbc"
