Sloppy
======

A collection of projects that use [OptSlope](https://gitlab.com/elad.noor/optslope).
To run the scripts in this repository, one must first install the `optslope` package.
The easiest option is PyPI:
```bash
pip install optslope
```

Otherwise, to install from source:
```bash
git clone https://gitlab.com/elad.noor/optslope.git
cd optslope
python setup.py install
```

## RuBisCO dependent E. coli
This algorithm was used by [Antonovsky et al.](https://doi.org/10.1016/j.cell.2016.05.064)
to find knockout combinations that couple *E. coli* growth to the flux in RuBisCO.

## Methanol dependent E. coli
Similarly, this algorithm was used by [Meyer et al.](https://doi.org/10.1038/s41467-018-03937-y) and in
[Keller et la.](https://doi.org/10.1038/s41467-020-19235-5)
to design knockouts that couple the growth of *E. coli* to methanol, as an additional
carbon source on top of more complex carbohydrates.

## E. coli as a glycerate biosensor
A recent project conducted by [Aslan et al.](https://doi.org/10.1016/j.ymben.2019.09.002), used OptSlope to design knock-out
combinations that have varying dependencies on glycerate in the media.

## Other references
A recent publication by [Jensen et al.](https://www.sciencedirect.com/science/article/pii/S2214030118300373)
uses a similar approach for strain design

# How to start your one OptSlope project

## Files that you'll need:
- `calculate slopes.ipynb`
- `check_autotrophic_growth_growth.py` - which is called by the first script
- `core_model_extended.xml` - this file includes all the substrates and metabolic reactions that the model takes into account.

## Before you start with the script:
Open the `.xml` file and check it includes all the substrates and reactions of interest. Under the list of species section see the metabolites of interest are present.
Here are a few explanations of suffix and prefix:
- Prefixes: `C_` - compartment, `M_` - metabolite, `R_` - reaction, `G_` - gene
- Suffixes: `_c` - cytosol, `_e` - extracellular, `_p` - periplasm
- For example: `M_etoh_e` means Metabolite_ethanol_extracellular

Here is an example for adding metabolite:  
```html
<listOfSpecies>
<species boundaryCondition="false" compartment="C_c" constant="false" hasOnlySubstanceUnits="false" initialConcentration="0" id="M_13dpg_c" name="13dpg_c" fbc:chemicalFormula="C3H4O10P2" />
```
If you add a new metabolite be sure to add an import reaction in the model in the section of reactions where the metabolites move from one compartment to another - see below.

Here is an example of adding a reaction: 
```html
<listOfReactions>
   <reaction fast="false" id="R_RBC" reversible="false" fbc:lowerFluxBound="cobra_0_bound" fbc:upperFluxBound="cobra_default_ub" name="rubisco">
        <listOfReactants>
          <speciesReference constant="true" species="M_rubp__D_c" stoichiometry="1" />
          <speciesReference constant="true" species="M_h2o_c" stoichiometry="1" />
          <speciesReference constant="true" species="M_co2_c" stoichiometry="1" />
        </listOfReactants>
        <listOfProducts>
          <speciesReference constant="true" species="M_3pg_c" stoichiometry="2" />
          <speciesReference constant="true" species="M_h_c" stoichiometry="3" />
        </listOfProducts>
```
if a reaction is reversible it will look as such: 
`reversible="true" fbc:lowerFluxBound="cobra_default_lb" fbc:upperFluxBound="cobra_default_ub"`
(no negative values for lower bound means irreversible)

If you are missing information about the reaction go to the [BiGG database](http://bigg.ucsd.edu/) to look for it.

Save the model and name it accordingly. For example: `core_model_extended_no_glx_shunt.xml`

## Changes to the *E. coli* core model:
We made the following changes to the E. coli core mode <REFERENCE>:
- Adding the Entner-Doudoroff pathway (EDA and EDD)
- Adding import reactions for: formate, glycerol, fructose, lactate, succinate, glycerate
- Making the trans-hydrogenase reaction (NADTRHD) reversible
- Adding the reverse reaction for GND (denoted GND_r)
- Adding the NAD-dependent formate dehydrogenase reaction (FDH)

## Open the jupyter notebook `calculate slopes.ipynb`:
- Define how many knockouts are acceptable as an output by the model
- the more knockouts the more time it will run
- We chose - `max_knockouts = 3`
- Define a max amount of workers meaning processors to run the script - depends on your computer
- choose target reaction - for example: `TARGET_REACTION = "RBC"`
- load the model as it was saved after editing - example: `WILDTYPE_MODEL = Path("core_model_extended_no_glx_shunt.xml")`

Define the relevant carbon sources relevant for selection and the gene pool available for knockout:
more genes and more carbon sources will take a longer processing time.

example:
```python
CARBON_SOURCES_LIST = [ 
    (
        "r5p","for"
    ),
...
]

SINGLE_KOS = [
    "RPI", 
...
]
```

The first carbon source is to test dependency on the reaction, the second is meant to test if it could be grown without the sugar selected doing so by calling the second script (`check_autotrophic_growth.py`)
For example, testing rbc on r5p will result with a drpi knockout selection system - but when testing for potential growth without r5p and only formate it will show it is impossible and will be omitted.

after defining the different parameters - run the calculate `slopes.ipynb` script. The result will be two files:
- `results_full_<n_knockouts>KOs.csv` - all the possible knockouts and carbon sources combinations and their slopes - the higher values for the slope the stronger the dependency. Units are: mmol of carbon source per grams of dry biomass.
- `results_summary_<n_knockouts>KOs.csv` - only shows combinations of knockouts that had a slope on one or more of the knockouts.
